//
//  TiempoViewModel.swift
//  ElTiempo
//
//  Created by Jesús Gallego Irles on 21/2/17.
//  Copyright © 2017 Universidad de Alicante. All rights reserved.
//

import Foundation
import Bond

class TiempoViewModel {
    let modelo = TiempoModelo()
    var estado = Observable<String>("")
    var icono = Observable<String>("")
    
    func consultarTiempo(de localidad: String) {
        modelo.consultarTiempo(localidad: localidad) {
            newEstado, newIcono in
            
            self.estado.value = newEstado
            self.icono.value = newIcono
        }
    }
}
